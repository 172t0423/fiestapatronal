package com.example.fiestapatronal

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_menu.*

class Menu : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_menu)
        fm.setOnClickListener {
            startActivity(Intent(this, opcion1::class.java))
        }
        fc.setOnClickListener {
            startActivity(Intent(this, opcion2::class.java))
        }
        cs.setOnClickListener {
            startActivity(Intent(this, login::class.java))
        }
        Creditos.setOnClickListener {
            startActivity(Intent(this, creditos::class.java))
        }
    }
}